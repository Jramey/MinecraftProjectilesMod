# MinecraftProjectilesMod
Minecraft mod to add various projectiles into the game

This is a forge mod that currently supports minecraft 1.8.  You must have forge installed for 1.8.  The mod jar is under
build/libs.
